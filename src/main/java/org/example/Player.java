package org.example;

import org.example.enums.PieceColor;

public class Player {
    private String name;
    private PieceColor color;

    public Player(String name, PieceColor color) {
        this.name = name;
        this.color = color;
    }

    public void makeMove(Square from, Square to, Board board) {
    }

}
