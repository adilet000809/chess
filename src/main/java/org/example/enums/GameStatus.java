package org.example.enums;

public enum GameStatus {
    ACTIVE, CHECK, CHECKMATE, MATE
}
