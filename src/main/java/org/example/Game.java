package org.example;

import org.example.enums.GameStatus;
import org.example.enums.PieceColor;

public class Game {
    private Board board;
    private Player currentPlayer;
    private GameStatus status;

    public Game() {
        this.board = new Board();
        this.currentPlayer = new Player("Player1", PieceColor.WHITE);
        this.status = GameStatus.ACTIVE;
    }

    public void startGame() {
    }

    public void endGame() {
    }

    public void switchPlayer() {
    }
}
