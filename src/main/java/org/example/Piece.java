package org.example;

import org.example.enums.PieceColor;
import org.example.enums.PieceType;

public class Piece {
    private PieceType type;
    private PieceColor color;
    private boolean isCaptured;

    public Piece(PieceType type, PieceColor color) {
        this.type = type;
        this.color = color;
        this.isCaptured = false;
    }

    public boolean isValidMove(Square from, Square to, Board board) {
        return true;
    }
}
